var btn = $('#toggle-cart');
var cart = $('#cart');


if(btn!=null)
btn.on('click', function () {
   cart.toggleClass('transformed');
});

var table = $('#table-container');
if(table!=null){
    table.on('click', function (e) {
        if(e.target.getAttribute('class')==='remove'){
            $('#myModal').modal('toggle');
        }
    })
}

var btnClose = $('#btn-close-cart');
if(btnClose!=null){
    btnClose.on('click', function () {
        cart.removeClass('transformed');
    })
}


var dropdown = $('#dropdown'),
    overlay = $('body');

if (dropdown.length){
    overlay.on('click', function (e) {

        if($(e.target).parents('.dropdown').length ){
            //console.log($(e.target).is('li'));
            dropdown.toggleClass('dropdown--visible ');
        } else{
            //console.log(e.target);
            dropdown.removeClass('dropdown--visible');
        }
    });
}

var switcher = $("#switcher");
if(switcher!=null){
    switcher.on('click', function () {
        $('#grid').toggleClass('active');
        $('#list').toggleClass('active');

        if($('#list').hasClass('active')){
            $('#view-switcher').removeClass('dataList--list').addClass('dataList--grid');

        }else{
            $('#view-switcher').removeClass('dataList--grid').addClass('dataList--list');
        }
    });
}

var modalButtons = [$("#btn-modal-cansel"), $("#btn-modal-confirm")];
modalButtons.forEach(function (item) {
    item.on('click', function () {
        $('#myModal').modal('toggle');
    })
});
console.log(modalButtons);

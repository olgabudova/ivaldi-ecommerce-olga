<div class="cart" id="cart">
    <div class="cart__container">
        <button class=" remove" data-toggle="modal" id="btn-close-cart">
            <span class="first-line"></span>
            <span class="second-line"></span>
        </button>
        <div class="header-cart-container">
            <div class="title-left">
                <section class="title-container">
                    <h3>Cart</h3>
                    <div class="subtitle">3 Items</div>
                </section>
            </div>
            <div class="btn-container">
                <button class=" btn-text">edit</button>
            </div>
        </div>
    </div>
    <div class="table-container">
        <table class="table">
            <tr>
                <td class="name__container">
                    <div class="name">Drill</div>
                    <div class="model subtitle">3RF-A</div>
                </td>
                <td class="amount"> <input type="number" value="2"></td>
                <td class="nok">122 NOK</td>
            </tr>
            <tr>
                <td class="name__container">
                    <div class="name">Drill</div>
                    <div class="model subtitle">3RF-A</div>
                </td>
                <td class="amount"> <input type="number" value="3"></td>
                <td class="nok">645 NOK</td>
            </tr>
            <tr>
                <td class="name__container">
                    <div class="name">Drill</div>
                    <div class="model subtitle">3RF-A</div>
                </td>
                <td class="amount"> <input type="number" value="1"></td>
                <td class="nok">65 NOK</td>
            </tr>
            <tr>
                <td class="name__container">
                    <div class="name">Drill</div>
                    <div class="model subtitle">3RF-A</div>
                </td>
                <td class="amount"> <input type="number" value="1"></td>
                <td class="nok ">242 NOK</td>
            </tr>
            <tr>
                <td class="name__container">
                    <div class="name">Drill</div>
                    <div class="model subtitle">3RF-A</div>
                </td>
                <td class="amount"> <input type="number" value="1"></td>
                <td class="nok">240 NOK</td>
            </tr>
        </table>
    </div>
    <div class="cart__container cart-editing">
        <div class="summary">
            <div class="subtitle">Totalt</div>
            <div class="sum">1,024 NOK</div>
        </div>
        <div class="btn-container inverse">
            <button class="btn-text">Confirm order</button>
        </div>
    </div>
        <div class="cart__container">
            <section class="title-container text-container">
                <h3 >Cart</h3>
                <div class="subtitle">3 Items</div>
            </section>
            <section class="title-container text-container">
                <h3 >Order confirmed</h3>
                <div class="subtitle">Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nullam turpis mauris,
                    elementum et ullamcorper sed, faucibus vestibulum sem. Cras vulputate sodales tellus vitae ullamcorper.</div>
            </section>
        </div>
        <section class="table-container">
            <table class="table">
                <tr>
                    <td class="name__container">
                        <div class="name">Drill</div>
                        <div class="model subtitle">3RF-A</div>
                    </td>
                    <td class="amount">x3</td>
                    <td class="nok">122 NOK</td>
                </tr>
                <tr>
                    <td class="name__container">
                        <div class="name">Drill</div>
                        <div class="model subtitle">3RF-A</div>
                    </td>
                    <td class="amount">x3</td>
                    <td class="nok">122 NOK</td>
                </tr>
                <tr>
                    <td class="name__container">
                        <div class="name">Drill</div>
                        <div class="model subtitle">3RF-A</div>
                    </td>
                    <td class="amount">x3</td>
                    <td class="nok">122 NOK</td>
                </tr>
                <tr>
                    <td class="name__container">
                        <div class="name">Drill</div>
                        <div class="model subtitle">3RF-A</div>
                    </td>
                    <td class="amount">x3</td>
                    <td class="nok ">122 NOK</td>
                </tr>
                <tr>
                    <td class="name__container">
                        <div class="name">Drill</div>
                        <div class="model subtitle">3RF-A</div>
                    </td>
                    <td class="amount">x3</td>
                    <td class="nok">122 NOK</td>
                </tr>
                <tr>
                    <td class="name__container">
                        <div class="name">Drill</div>
                        <div class="model subtitle">3RF-A</div>
                    </td>
                    <td class="amount">x3</td>
                    <td class="nok">122 NOK</td>
                </tr>
            </table>
        </section>
    <div class="cart__container">
        <div class="header-cart-container">
            <div class="title-left">
                <section class="title-container">
                    <h3>Cart edited</h3>
                    <div class="subtitle">3 Items</div>
                </section>
            </div>
            <div class="btn-container">
                <button class=" btn-text color-grey">CANSEL</button>
            </div>
        </div>
    </div>
    <div class="table-container" id="table-container">
        <table class="table">
            <tr>
                <td class="name__container">
                    <div class="name">Drill</div>
                    <div class="model subtitle">3RF-A</div>
                </td>
                <td class="amount">x3</td>
                <td class="nok">122 NOK</td>
                <td>
                    <button class="remove" data-toggle="modal">
                        <span class="first-line"></span>
                        <span class="second-line"></span>
                    </button>
                </td>
            </tr>
            <tr>
                <td class="name__container">
                    <div class="name">Drill</div>
                    <div class="model subtitle">3RF-A</div>
                </td>
                <td class="amount">x3</td>
                <td class="nok">122 NOK</td>
                <td>
                    <button class="remove" data-toggle="modal">
                        <span class="first-line"></span>
                        <span class="second-line"></span>
                    </button>
                </td>
            </tr>
            <tr>
                <td class="name__container">
                    <div class="name">Drill</div>
                    <div class="model subtitle">3RF-A</div>
                </td>
                <td class="amount">x3</td>
                <td class="nok">122 NOK</td>
                <td>
                    <button class="remove" data-toggle="modal">
                        <span class="first-line"></span>
                        <span class="second-line"></span>
                    </button>
                </td>
            </tr>
            <tr>
                <td class="name__container">
                    <div class="name">Drill</div>
                    <div class="model subtitle">3RF-A</div>
                </td>
                <td class="amount">x3</td>
                <td class="nok ">122 NOK</td>
                <td>
                    <button class="remove" data-toggle="modal">
                        <span class="first-line"></span>
                        <span class="second-line"></span>
                    </button>
                </td>
            </tr>
            <tr>
                <td class="name__container">
                    <div class="name">Drill</div>
                    <div class="model subtitle">3RF-A</div>
                </td>
                <td class="amount">x3</td>
                <td class="nok">122 NOK</td>
                <td>
                    <button class="remove" data-toggle="modal" >
                        <span class="first-line"></span>
                        <span class="second-line"></span>
                    </button>
                </td>
            </tr>
            <tr>
                <td class="name__container">
                    <div class="name">Drill</div>
                    <div class="model subtitle">3RF-A</div>
                </td>
                <td class="amount">x3</td>
                <td class="nok">122 NOK</td>
                <td>
                    <button class="remove" data-toggle="modal">
                        <span class="first-line"></span>
                        <span class="second-line"></span>
                    </button>
                </td>
            </tr>
        </table>
        <div class="cart__container">
            <div class="summary">
                <div class="subtitle">Totalt</div>
                <div class="sum">1,024 NOK</div>
            </div>
        </div>
    </div>
</div>

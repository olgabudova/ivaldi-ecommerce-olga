<header>
    <div class="container">
        <div class="row align-items-center header__height justify-content-between">

            <a href="../index.html" class="">
                <img src="./images/ivaldi_logo_black.svg" alt="">
            </a>

            <div class="account">
                <div class="dropdown" id="dropdown">
                    <div class=dropdown__header>Michael Nino Evensen</div>
                    <ul class="dropdown__body">
                        <li>Action</li>
                        <li>Something</li>
                        <li>Settings</li>
                    </ul>
                </div>
                <div class="basket" id="toggle-cart">
                    <i class="fa fa-shopping-cart" aria-hidden="true"></i>
                </div>
            </div>
        </div>
        </div>
</header>
@@include('./cart.tpl')
@@include('./modal.tpl')